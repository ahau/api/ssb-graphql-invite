const { GraphQLError } = require('graphql')
const { promisify: p } = require('util')
const pull = require('pull-stream')
const { where, type, toPullStream } = require('ssb-db2/operators')

function block (id) {
  return {
    type: 'contact', contact: id, blocking: true
  }
}

function unblock (id) {
  return {
    type: 'contact', contact: id, blocking: false
  }
}

module.exports = function Resolvers (ssb) {
  if (!ssb) throw new Error('invalid sbot')
  if (!ssb.invite) throw new Error('requires ssb-invite')

  async function isPataka (feedId) {
    const pubAnnounces = await pull(
      ssb.db.query(where(type('pub')), toPullStream()),
      pull.filter(m => m.value.content?.address?.key === feedId),
      pull.take(1),
      pull.collectAsPromise()
    )

    return pubAnnounces.length > 0
  }

  return {
    Mutation: {
      createInvite (_, { input }) {
        return p(ssb.invite.create)({ ...input })
          .catch(err => new GraphQLError(err))
      },

      acceptInvite (_, { inviteCode }) {
        if (!inviteCode) return Promise.reject(new GraphQLError('Must provide an inviteCode'))

        return p(ssb.invite.accept)(inviteCode)
          .catch(err => new GraphQLError(err))
          .then(getPatakaFeedId)
      },

      async blockPataka (_, { feedId }) {
        const isValidFeedId = await isPataka(feedId)
        if (!isValidFeedId) return new GraphQLError('cannot block feedId, it is not a pataka')

        return p(ssb.db.create)({
          content: {
            ...block(feedId),
            recps: [ssb.id]
          },
          encryptionFormat: 'box2'
        })
          .then(msg => msg.key)
          .catch(err => new GraphQLError(err))
      },

      async unblockPataka (_, { feedId }) {
        const isValidFeedId = await isPataka(feedId)
        if (!isValidFeedId) return new GraphQLError('cannot block feedId, it is not a pataka')

        return p(ssb.db.create)({
          content: {
            ...unblock(feedId),
            recps: [ssb.id]
          },
          encryptionFormat: 'box2'
        })
          .then(msg => msg.key)
          .catch(err => new GraphQLError(err))
      }
    }
  }
}

function getPatakaFeedId (msgs) {
  const pubMsg = msgs.find(m => m.value.content.type === 'pub')
  if (pubMsg) return pubMsg.value.content.address.key

  const contactMsg = msgs.find(m => m.value.content.type === 'contact')
  if (contactMsg) return contactMsg.value.content.contact
}
