const { gql } = require('graphql-tag')

module.exports = gql`
  extend type Mutation {
    """
    Create a ssb pataka invite
    """
    createInvite(input: createInviteInput): String

    """
    Accept a ssb pataka invite
    """
    acceptInvite(inviteCode: String!): String

    """
    Block a pataka
    """
    blockPataka(feedId: String!): String

    """
    Unblock a pataka
    """
    unblockPataka(feedId: String!): String
  }

  input createInviteInput {
    uses: Int
    note: String
    external: String
    modern: Boolean
  }

`
