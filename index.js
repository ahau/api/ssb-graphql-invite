const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')

module.exports = sbot => ({
  typeDefs,
  resolvers: Resolvers(sbot)
})
